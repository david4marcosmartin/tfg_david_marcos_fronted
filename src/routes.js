import React from 'react';
import $ from 'jquery';

window.jQuery = $;
window.$ = $;
global.jQuery = $;



const TableGeneralData = React.lazy(() => import('./Demo/Tables/TableGeneralData'));
const TrendingTopics = React.lazy(() => import('./Demo/Tables/TrendingTopics'));
const TableChannels = React.lazy(() => import('./Demo/Tables/TableChannels'));
const OldNews = React.lazy(() => import('./Demo/Tables/OldNews'));
const Statistics = React.lazy(() => import('./Demo/Tables/Statistics'));

const News = React.lazy(() => import('./Demo/Tables/News'));


const routes = [
{ path: '/index', exact: true, name: 'TableGeneralData Table', component: TableGeneralData },
{ path: '/tables/TableChannels', exact: true, name: 'TableChannels Table', component: TableChannels },
{ path: '/tables/TrendingTopics', exact: true, name: 'TrendingTopics Table', component: TrendingTopics },
{ path: '/tables/News', exact: true, name: 'News Table', component: News },
{ path: '/tables/OldNews', exact: true, name: 'OldNews Table', component: OldNews },
{ path: '/tables/Statistics', exact: true, name: 'Statistics', component: Statistics },

];

export default routes;