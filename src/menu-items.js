export default {
    items: [
        
       
        {
            id: 'ui-forms',
            title: 'Information',
            type: 'group',
            icon: 'icon-group',
            children: [
                
                {
                    id: 'bootstrap',
                    title: 'Latest news',
                    type: 'item',
                    icon: 'feather icon-server',
                    url: '/tables/TableGeneralData'
                },
                {
                    id: 'bootstrap', 
                    title: 'Trending topics',
                    type: 'item',
                    icon: 'feather icon-trending-up',
                    url: '/tables/TrendingTopics'
                },
                {
                    id: 'bootstrap',
                    title: 'Old news',
                    type: 'item',
                    icon: 'feather icon-server',
                    url: '/tables/OldNews'
                },
                
                {
                    id: 'bootstrap',
                    title: 'Statistics',
                    type: 'item',
                    icon: 'feather icon-pie-chart',
                    url: '/tables/Statistics'
                },

                {
                    id: 'bootstrap',
                    title: 'Channels',
                    type: 'item',
                    icon: 'fa fa-send',
                    url: '/tables/TableChannels'
                },
                
            ]
        },

    ]
}