import React from 'react';
import {
  Col,
  Card,
} from 'react-bootstrap';
import '../../routes.js';
import { MDBDataTableV5 } from 'mdbreact';
import Chart from "react-apexcharts";
import "@fortawesome/fontawesome-free/css/all.min.css";



class TrendingTopics extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };

    this.getData = this.getData.bind(this);

  }

  async getData() {
    const res = await fetch(`http://localhost:8001/topics`);
    const data = await res.json();
  
    return data;
  }

  async componentDidMount() {
    const data = await this.getData();
    this.setState({ data });
  }

  createRows() {

    let rows = [];
    var data = this.state.data
   
    for (let x in data.topics) {

      const options ={
        chart: {
          id: "basic-bar",
          type: 'bar',
          width: 150,
          height: 35,
          sparkline: {
            enabled: true
          }
        },
        plotOptions: {
          bar: {
            columnWidth: '80%'
          }
        },
        xaxis: {
          crosshairs: {
            width: 1
          },
        },
        stroke :{ width: 0.9,
          curve: 'straight'},
        tooltip: {
          fixed: {
            enabled: false
          },
          x: {
            show: false
          },
          y: {
            title: {
              formatter: function (seriesName) {
                return ''
              }
            }
          },
          marker: {
            show: true
          }
        }
      
      
        
      }
    
  

    const series = [{
        name: "",
        data: data.topics[x].chart_data
    }]

      var item = {
        'topic': data.topics[x].topic,
        'channels': data.topics[x].channels,
        'chart':[<div className='chart_table'><Chart options={options} series={series} type="line" height={35} width={150} /></div>
            ]
      }
      rows.push(item);
    }

    return rows
  }

  render() {

    const data = {
      columns: [
        {
          label: [<div className='head_table_channels'>Topic</div>],
          field: 'topic',
          sort: 'disabled',
          width: 100

        },
        {
          label: [<div className='head_table_channels'>Last 48 hours</div>],
          field: 'chart',
          sort: 'disabled',
          width: 100
        },
        {
          label: [<div className='head_table_channels'>Channels</div>],
          field: 'channels',
          sort: 'disabled',
          width: 100
        },
      
      ],
      rows: this.createRows()
    }

    return (
      <div>
        <Col md={6} xl={12}>
          <Card className='Recent-Users'>
            <Card.Header>
              <Card.Title as='h5'>TRENDING TOPICS</Card.Title>
            </Card.Header>
            <Card.Body className='px-0 py-2'>
              <MDBDataTableV5
                hover
                order={[0, 'desc']}
                responsive
                searchBottom={false}
                paging={true}
                striped
                bordered
                data={data}
              />
            </Card.Body>
          </Card>
        </Col>
      </div>
    );
  }
}

export default TrendingTopics;

