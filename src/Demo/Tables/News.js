import React from 'react';
import { Row, Col, Card, Table } from 'react-bootstrap';
import Aux from "../../hoc/_Aux";
import { Dropdown } from 'react-bootstrap';


class News extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            info: [],
            id: "",
            old: false,
        };



        if (this.props.location.aboutProps) {
            this.state.id = this.props.location.aboutProps.id;
            localStorage.setItem('id', this.props.location.aboutProps.id);
            this.state.old = this.props.location.aboutProps.old;
        } else {
            this.state.id = localStorage.getItem("id");

        }

    }

    async getTweetData() {

        var url = null

        if (this.state.old === false) {
            url = `http://localhost:8001/news/` + this.state.id;

        } else {
            url = `http://localhost:8001/old/news/` + this.state.id;
        }
        console.log(url)

        const res = await fetch(url);
        const data = await res.json();

        return data;
    }

    async componentDidMount() {
        const info = await this.getTweetData();
        this.setState({ info });
    }





    render() {
        let dropdownRightAlign = false;
        if (this.props.rtlLayout) {
            dropdownRightAlign = true;
        }

        return (

            <div>
                {this.state.info.map((data, index) => (

                    <div key={index}>
                        <Aux>
                            <a href={data.url} className="m-0" target="_blank" rel="noopener noreferrer"> <h2>{data.title}</h2></a>
                            <div className='subtitle'>
                                <h7 >Published: {data.date} </h7>  {!this.state.old &&
                                                                <h7 className='subtitle_left'>Active time: {data.time_in}</h7>
                                                            }
                            </div>


                            <Row>
                                <Col md={6} xl={4}>
                                    <Card className='card-social'>
                                        <Card.Body className='border-bottom'>
                                            <div className="row align-items-center justify-content-center">
                                                <div className="col-auto">
                                                    <i className="feather icon-repeat text-c-blue f-36" />
                                                </div>
                                                <div className="col text-right">
                                                    <h3>{data.rt}</h3>
                                                    <h5 className="text-c-green mb-0"><span className="text-muted">Total Retweets</span></h5>
                                                </div>
                                            </div>
                                        </Card.Body>

                                    </Card>
                                </Col>
                                <Col md={6} xl={4}>
                                    <Card className='card-social'>
                                        <Card.Body className='border-bottom'>
                                            <div className="row align-items-center justify-content-center">
                                                <div className="col-auto">
                                                    <i className="fa fa-heart text-c-red f-36" />
                                                </div>
                                                <div className="col text-right">
                                                    <h3>{data.favs}</h3>
                                                    <h5 className="text-c-purple mb-0"><span className="text-muted">Total Likes</span></h5>
                                                </div>
                                            </div>
                                        </Card.Body>

                                    </Card>
                                </Col>
                                <Col xl={4}>
                                    <Card className='card-social'>
                                        <Card.Body className='border-bottom'>
                                            <div className="row align-items-center justify-content-center">
                                                <div className="col-auto">
                                                    <i className="fa fa-send text-c-blue f-36" />
                                                </div>
                                                <div className="col text-right">
                                                    <h3>{data.num_channels}</h3>
                                                    <h5 className="text-c-blue mb-0"><span className="text-muted">Channels</span></h5>
                                                </div>
                                            </div>
                                        </Card.Body>

                                    </Card>
                                </Col>
                            </Row>

                        </Aux>

                        <Row>
                            <Col md={6} xl={8}>
                                <Card className='Recent-Users'>
                                    <Card.Header>
                                        <Card.Title as='h5'>Tweets</Card.Title>
                                    </Card.Header>
                                    <Card.Body className='px-0 py-2'>
                                        <Table responsive hover>
                                            <tbody>
                                                {data.twitter.map((tweet, t) => (

                                                    <div key={t}>
                                                        
                                                        <tr className="unread">
                                                            <td><img className="rounded-circle" style={{ width: '40px', display: 'block' }} src={tweet.profile_image} alt="user" />

                                                            </td>
                                                            <td className='profile_twitter'>
                                                                <a href={tweet.profile_url} className="m-0" target="_blank" rel="noopener noreferrer"><h7 className="mb-1">{tweet.name}</h7></a>
                                                                <p className="m-0"><i className="feather icon-repeat text-c-blue f-15" />{tweet.retweet} RTS</p>
                                                                <p className="m-0"><i className="fa fa-heart text-c-red f-15" />{tweet.favorites} FAVS</p>

                                                            </td>
                                                           

                                                            
                                                                <div className='text_tweet'
                                                                ><p >{tweet.text} <h7 className="mb-1"><a href={tweet.link} className="m-0" target="_blank" rel="noopener noreferrer">See Tweet</a></h7> </p></div>
                                                            

                                                        </tr>

                                                    </div>

                                                ))}

                                            </tbody>
                                        </Table>
                                    </Card.Body>
                                </Card>
                            </Col>

                            <Col md={6} xl={4}>
                                <Card className='Recent-Users'>
                                    <Card.Header>
                                        <Card.Title as='h5'>Telegram channels</Card.Title>
                                    </Card.Header>
                                    <Card.Body className='px-0 py-2'>
                                        <Table responsive hover>
                                            <tbody>
                                                {data.channels.map((channel, t) => (

                                                    <div key={t}>
                                                        <tr className="unread">
                                                            <td><img className="rounded-circle" style={{ width: '40px' }} src={channel.profile_image} alt="channel" /></td>
                                                            <td>
                                                                <h7 className="mb-1"><a href={channel.profile} className="m-0" target="_blank" rel="noopener noreferrer">{channel.name}</a></h7>


                                                            </td>


                                                        </tr>
                                                    </div>
                                                ))}

                                            </tbody>
                                        </Table>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>


                    </div>
                ))}


            </div>
        );
    }
}

export default News;