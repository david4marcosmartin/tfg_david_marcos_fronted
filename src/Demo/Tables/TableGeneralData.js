import React from 'react';
import {
  Col,
  Card,
} from 'react-bootstrap';
import '../../routes.js';
import { MDBDataTableV5 } from 'mdbreact';
import { NavLink } from 'react-router-dom';
import "@fortawesome/fontawesome-free/css/all.min.css";


class TableGeneralData extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };

    this.getData = this.getData.bind(this);

  }

  async getData() {
    const res = await fetch(`http://localhost:8001/data`);
    const data = await res.json();
    console.log("data" + data)

    return data;
  }

  async componentDidMount() {
    const data = await this.getData();
    this.setState({ data });

  }

  createRows() {

    let rows = [];
    var data = this.state.data

    for (let x in data) {

      var item = {
        'num_rt': data[x].num_rt,
        'num_favs': data[x].num_favs,
        'date': data[x].date,

        'title': [

          <div className="table_title">
            <a href={data[x].url} className="m-0" target="_blank" rel="noopener noreferrer"><h6 className="mb-1">{data[x].title}</h6></a>


          </div>
        ],
        'info': [<div className="center_button"><NavLink to={{ pathname: "/Tables/News", aboutProps: { id: data[x].id, old: false } }}><i className="feather icon-info text-c-blue f-55" /></NavLink></div>]

      }

      rows.push(item);
    }

    return rows
  }

  render() {

    const data = {
      columns: [
        {
          label: [<div className='head_table_date'>Date   <i className="feather icon-calendar text-c-blue f-15" /></div>],
          field: 'date',
          width: 100

        },
        {
          label: [<div className='head_table_icons'><i className="feather icon-repeat text-c-blue f-5" /></div>],
          field: 'num_rt',
          width: 100

        },
        {
          label: [<div className='head_table_icons'> <i className="fa fa-heart text-c-red f-8" /></div>],
          field: 'num_favs',
          width: 100
        },

        {
          label: [<div className='head_table'><i className="feather icon-info text-c-blue f-15" /></div>],
          field: 'info',
          sort: 'disabled',
          width: 40

        },
        {
          label: [<div className='head_table'>News</div>],
          field: 'title',
          sort: 'disabled',

          width: 100
        }
      ],
      rows: this.createRows()
    }

    return (
      <div>
        <Col md={6} xl={12}>
          <Card className='Recent-Users'>
            <Card.Header>
              <Card.Title as='h5'>RANKING URLS</Card.Title>
            </Card.Header>
            <Card.Body className='px-0 py-2'>
              <MDBDataTableV5
                hover
                order={[0, 'desc']}
                responsive
                searchBottom={false}
                paging={true}
                striped
                bordered
                data={data}
              />
            </Card.Body>
          </Card>
        </Col>
      </div>
    );
  }
}

export default TableGeneralData;

