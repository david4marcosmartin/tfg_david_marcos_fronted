import React from 'react';
import {
  Col,
  Card,
} from 'react-bootstrap';
import '../../routes.js';
import { MDBDataTableV5 } from 'mdbreact';
import "@fortawesome/fontawesome-free/css/all.min.css";



class TableChannels extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };

    this.getData = this.getData.bind(this);

  }

  async getData() {
    const res = await fetch(`http://localhost:8001/channels`);
    const data = await res.json();

    return data;
  }

  async componentDidMount() {
    const data = await this.getData();
    this.setState({ data });

  }

  createRows() {

    let rows = [];
    var data = this.state.data

    for (let x in data) {

      var item = {
        'news': data[x].last_week,
        'active_news': data[x].active_news,
     

        'image': [<img className="rounded-circle" style={{ width: '40px' }} src={data[x].profile_image} alt="channel" />],
        'channels': [

          <h7 className="mb-1"><a href={data[x].url} className="m-0" target="_blank" rel="noopener noreferrer">{data[x].channel}</a></h7>
        ],
        'topic_channel': [<p>{data[x].topic_channel}</p>],
     

      }
      rows.push(item);

    }

    return rows
  }

  createString(num) {
    console.log("'" + num.toString() + "%'")
    return num.toString() + "%"

  }

  render() {

    const data = {
      columns: [
        {
          label: [<div className='head_table_channels'></div>],
          field: 'image',
          sort: 'disabled',

          width: 100

        },
        {
          label: [<div className='head_table_channels'>Channels</div>],
          field: 'channels',
          sort: 'disabled',

          width: 100

        },
        {
          label: [<div className='head_table_channels'>News in last week</div>],
          field: 'news',

          width: 100

        },
        {
          label: [<div className='head_table_channels'>News in the last 48h</div>],
          field: 'active_news',

          width: 100

        },
        {
          label: [<div className='head_table'>Trending topics</div>],
          field: 'topic_channel',

          width: 100

        },

      ],
      rows: this.createRows()
    }

    return (
      <div>
        <Col md={6} xl={12}>
          <Card className='Recent-Users'>
            <Card.Header>
              <Card.Title as='h5'>RANKING CHANNEL NEWS</Card.Title>
            </Card.Header>
            <Card.Body className='px-0 py-2'>
              <MDBDataTableV5
                hover
                order={[0, 'desc']}
                responsive
                searchBottom={false}
                paging={true}
                striped
                bordered
                data={data}
              />
            </Card.Body>
          </Card>
        </Col>
      </div>
    );
  }
}

export default TableChannels;

