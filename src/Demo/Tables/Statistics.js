import React from 'react';
import { Row, Col, Card, Table } from 'react-bootstrap';
import Chart from "react-apexcharts";


class Statistics extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            channels: [],
            num_channels: 0,
            num_posts: 0,
            num_active_posts: 0,
            pertenage_active:0,
            percentage_channels: 0,
            percentage: 0,
            chart_data: [],
            chart_keys: [],

        };


    }

    async get_statistics() {

        const url = 'http://localhost:8001/statistics'

        const res = await fetch(url);
        const data = await res.json();
        this.state.data_channels = data.data_channels
        this.state.num_channels = data.num_channels
        this.state.num_posts = data.num_posts
        this.state.num_active_posts = data.num_active_posts
        this.state.chart_data = data.chart_data
        this.state.pertenage_active = data.pertenage_active
        this.state.percentage_channels = data.percentage_channels
        this.state.chart_keys = data.chart_keys
        this.state.percentage = data.percentage_active
       

        return data.channels;
    }

    async componentDidMount() {
        const channels = await this.get_statistics();
        this.setState({ channels });
    }

    createString(num) {
        console.log("'" + num.toString() + "%'")
        return  num.toString() + "%"
        
    }


    render() {

        const options = {
            chart: {
              toolbar: {
                show: false,
              },
              stacked: false,
            },
            dataLabels: {
              enabled: false,
            },
            stroke: {
              show: true,
              width: 4,
              colors: ["transparent"],
            },
            legend: {
              show: true,
            },
            plotOptions: {
              bar: {
                horizontal: false,
                columnWidth: "30%",
                borderRadius: 2,
              },
            },
            colors: ["#0d6efd", "#009efb", "#6771dc"],
            xaxis: {
              categories: this.state.chart_keys,
            },
            responsive: [
              {
                breakpoint: 1024,
                options: {
                  plotOptions: {
                    bar: {
                      columnWidth: "60%",
                      borderRadius: 7,
                    },
                  },
                },
              },
            ],
          };
          const series = [
            {
              name: "Number of news",
              data: this.state.chart_data,
            },
            
          ];
        
        var icon = <i className="feather icon-chevron-right text-c-blue f-30 m-r-5" />  

        if (this.state.percentage > 0) {
            icon = <i className="feather icon-arrow-up text-c-green f-30 m-r-5"/> 
        }else if(this.state.percentage < 0){
            icon = <i className="feather icon-arrow-down text-c-red f-30 m-r-5"/>
        }
        
        return (

            <Row>
                <Col md={6} xl={4}>
                    <Card>
                        <Card.Body>
                            <h6 className='mb-4'>Channels with news with at least 1 iteraction in Twitter</h6>
                            <div className="row d-flex align-items-center">
                                <div className="col-9">
                                    <h3 className="f-w-300 d-flex align-items-center m-b-0"><i className="feather icon-chevron-right text-c-blue f-30 m-r-5" /> {this.state.num_channels} </h3>
                                </div>

                                <div className="col-3 text-right">
                                    <p className="m-b-0">{this.state.percentage_channels}%</p>
                                </div>                               
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md={6} xl={4}>
                    <Card>
                        <Card.Body>
                            <h6 className='mb-4'>News in the last 48h</h6>
                            <div className="row d-flex align-items-center">
                                <div className="col-9">
                                    <h3 className="f-w-300 d-flex align-items-center m-b-0">{icon}
                                                            {this.state.num_active_posts}</h3>
                                </div>                               
                                    <div className="col-3 text-right">
                                        <p className="m-b-0">{this.state.percentage}%</p>
                                    </div>                               
                            </div>
                           
                        </Card.Body>
                    </Card>
                </Col>
                <Col md={6} xl={4}>
                    <Card>
                        <Card.Body>
                            <h6 className='mb-4'>Total news</h6>
                            <div className="row d-flex align-items-center">
                                <div className="col-9">
                                    <h3 className="f-w-300 d-flex align-items-center m-b-0"><i className="feather icon-chevron-right text-c-blue f-30 m-r-5" /> {this.state.num_posts}</h3>
                                </div>

                               
                            </div>
                           
                        </Card.Body>
                    </Card>
                </Col>
                <Col md={6} >
                    <Card className='Recent-Users'>
                        <Card.Header>
                            <Card.Title as='h5'>Telegram channels with more news with at least 1 iteraction in Twitter</Card.Title>
                        </Card.Header>
                        <Card.Body className='px-0 py-2 scroll_sta'>
                        <Table responsive hover>
                                            <tbody>
                                                {this.state.channels.map((channel, t) => (

                                                    <div key={t}>
                                                        <tr className="unread"> 
                                                            <td><img className="rounded-circle" style={{ width: '40px' }} src={channel.profile_image} alt="channel" /></td>
                                                            <td>
                                                                <h7 className="mb-1"><a href={channel.profile} className="m-0" target="_blank" rel="noopener noreferrer">{channel.name} </a>{channel.num} News</h7>
                        

                                                            </td>


                                                        </tr>
                                                    </div>
                                                ))}

                                            </tbody>
                                        </Table>
                        </Card.Body>
                    </Card>
                </Col>

                <Col md={6}>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">News in the last 48 hours</Card.Title>
                            </Card.Header>
                            <Card.Body className="text-center">
                            <Chart options={options} series={series} type="area" height="279" />
                            </Card.Body>
                        </Card>
                    </Col>
            </Row>

        )
    }
}

export default Statistics;